# Run SimpleCrudServer in local Kubernetes 

   * Install Minikube (https://github.com/kubernetes/minikube)
 
   * Start local Kubernetes (k8s)
```
minikube start
```

   * Deploy SimpleCrudServer into local k8s
```
./deploy
```

   * Check that pods were deployed
```
kubectl --all-namespaces=true get pods
```

   * Obtain ip of local k8s
```
minikube ip
```

   * Ping SimpleCrudServer
```
curl $(minikube ip):30061/persons
```

   * Undeploy SimpleCrudServer from local k8s
```
./undeploy
```

   * Stop local k8s
```
minikube stop
```


   * Delete Minikube instance
```
minikube delete
```

   * Create cluster
```
minikube start -p <name>
```

   * Show all clusters
```
kubectl config get-contexts
```

   * Show all services in namespace
```
kubectl get services --namespace=autodevelo-default
```


   * Change kubernetes cluster
```
kubectl config use-context CONTEXT_NAME
```




## Troubleshooting
### OS X

Complete restart

```
sh undeploy.sh && minikube stop && minikube delete && rm -rf ~/.kube ~/.minikube && minikube start && sh deploy.sh
```


## Minikube dashboard
```
minikube dashboard
```
